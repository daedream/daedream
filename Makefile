ARCH ?= x86_64
NAME=DaeDream

KERNDIR=kernel
KERN=$(KERNDIR)/kern.$(ARCH).elf

INITRDDIR=initrd
INITRD=$(IMGDIR)/initrd

BOOTLOADER = Limine

PWD=$(shell pwd)

IMGDIR=disk
TARGET=$(NAME).vhd
IMG=$(TARGET:.vhd=.img)

INFO="[34m[INFO][0m"

all: $(TARGET)

ifeq ($(ARCH),x86_64)
BASE_QEMU_FLAGS=-no-reboot -no-shutdown -s -S
QEMU_FLAGS=-serial stdio $(BASE_QEMU_FLAGS)
QEMU_FLAGS_NO_GRAPHICS=-nographic $(BASE_QEMU_FLAGS)
run-smp: $(TARGET)
	qemu-system-x86_64 -readconfig build/qemuconf-smp.conf $(QEMU_FLAGS)
run: $(TARGET)
	qemu-system-x86_64 -readconfig build/qemuconf.conf $(QEMU_FLAGS)
run_ng: $(TARGET)
	qemu-system-x86_64 -readconfig build/qemuconf.conf $(QEMU_FLAGS_NO_GRAPHICS)

debug:
	cd $(KERNDIR); gdb kern.x86_64.elf -x ../build/gdbexec
endif



$(TARGET): $(IMG)
	@echo $(INFO) Converting image
	@qemu-img convert -f raw -O vpc $^ $@

$(IMG): $(IMGDIR) $(INITRD) $(KERN)
	@echo $(INFO) Making raw image
	@cp $(KERN) $(IMGDIR)/
	@sh build/makedisk.sh $(IMGDIR) $(IMG) $(ARCH) $(BOOTLOADER)

$(INITRD): $(KERN) $(INITRDDIR)
	@echo $(INFO) Making initrd...
	@cp $(KERN) $(INITRDDIR)/
	@cd $(INITRDDIR); tar -H ustar -cf $(PWD)/$(INITRD) *; cd $(PWD)

$(KERN):
	@echo $(INFO) Buiding kernel...
	$(MAKE) -C $(KERNDIR)

$(INITRDDIR):
	@mkdir $@
$(IMGDIR):
	@mkdir $@

.PHONY: clean.kern
clean.kern:
	@make -C kernel clean

.PHONY: clean
clean: clean.kern
	@rm -r $(INITRDDIR) || echo "$(INITRDDIR) already removed"
	@rm -r $(IMGDIR) || echo "$(IMGDIR) already removed"
	@rm -r $(IMG) || echo "$(IMG) already removed"
	@rm -r $(TARGET)
