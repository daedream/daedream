#!/usr/bin/sh

diskdir=$1
imgname=$2
arch=$3
bootloader=$4
# God I hate bash sometimes. Stop doing crap that isn't POSIX compliant!
userid=${UID:-$(id -u)}

# Common useful funcitons
mkimg() {
    dd if=/dev/zero of=$imgname bs=1M count=100 status=noxfer
}

mk_BOOTBOOT() {
    # Used regardless of what the arch is
    echo "Moving the BOOTBOOT config to the disk directory"
    mkdir -p $diskdir/BOOTBOOT/
    cp build/config $diskdir/BOOTBOOT/CONFIG
    mk_btbt_$arch
}

mk_Limine() {
    # Used regardless of what the arch is
    echo "Moving the Limine config to the disk root"
    cp build/limine.cfg $diskdir/limine.cfg
    mk_limine_$arch
}

mk_efi_img() {
    echo "Making the image" 1>/dev/stderr
    mkimg
    echo "Partitioning the image" 1>/dev/stderr
    echo "g
    n
    1
    2048
    -0
    t
    1
    w" | fdisk $imgname > /dev/null # Partition the disk > /dev/null
    echo "Creating loop device (SUDO)" 1>/dev/stderr
    loopdev=$(sudo losetup --find)
    echo "Loop device: $loopdev" 1>/dev/stderr
    sudo losetup $(basename $loopdev) -P $imgname # Start up a loop device
    echo "Making file system (SUDO)" 1>/dev/stderr
    sudo mkfs.vfat ${loopdev}p1
    echo "Mounting file system (SUDO)" 1>/dev/stderr
    mkdir disktmp
    sudo mount ${loopdev}p1 disktmp -o uid=$userid
}

umount_efi_img() {
    echo "Unmounting file system (SUDO)" 1>/dev/stderr
    sudo umount ${loopdev}p1
    rmdir disktmp
    echo "Removing loop device (SUDO)" 1>/dev/stderr
    sudo losetup --detach ${loopdev}

}

mk_limine_x86_64() {
    echo "Setting up the disk directory" 1>/dev/stderr
    mkdir -p $diskdir/EFI/BOOT
    cp limine/BOOTX64.EFI $diskdir/EFI/BOOT/BOOTX64.EFI > /dev/null
    mk_efi_img
    cp -r $diskdir/* disktmp/
    umount_efi_img
}


# Define your arch specific instructions here
mk_btbt_x86_64() {
    echo "Setting up disk directory" 1>/dev/stderr
    mkdir -p $diskdir/EFI/BOOT
    cp bootboot/dist/bootboot.efi $diskdir/EFI/BOOT/BOOTX64.EFI > /dev/null
    mk_efi_img
    cp -r disk/* disktmp/
    umount_efi_img
}

mk_$bootloader > /dev/null

echo "Done creating $imgname"
